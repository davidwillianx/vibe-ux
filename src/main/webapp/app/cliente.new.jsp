<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.css">
    <link rel="stylesheet" href="<c:url value="/assets/styles.css"/> " >
    <title>Vibe - Cliente - Novo</title>
</head>
<body>
<div class="container">
    <div class="row">


        <div class="col s6 m6 offset-s3 offset-m3">

            <div class="card">

                <div class="card-content">
                    <div class="col m12 s12 vibe-logo">
                        <div class="col m4 s4 offset-m3 offset-s3">
                            <img src="<c:url value="/assets/imgs/vibe-logo.png"/>" alt="">
                        </div>
                    </div>

                    <span class="card-title">Novo Cliente</span>

                    <form id="cliente-new">
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Informe seu nome" name="Nome" id="nome" type="text"
                                       class="validate">
                                <label for="nome">Nome</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input type="text" id="cpf" value="${cliente.cpf}" name="Cpf" placeholder="Informe seu CPF">
                                <label for="cpf">Cpf</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" name="Email" value="${cliente.email}" type="email" class="validate"
                                       placeholder="Informe seu email">
                                <label for="email">Email</label>
                            </div>
                        </div>

                        <button class="btn waves-effect waves-light" type="submit" name="action">Registrar Cliente
                        </button>

                        <a class="btn-flat waves-effect waves-light pull-right" href="<c:url value="/" />">Procurar Cliente</a>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>


<div id="modal-info" class="modal">
    <div class="modal-content">
        <h4>Header</h4>
        <p>Description</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
    </div>
</div>





</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-serialize-object/2.5.0/jquery.serialize-object.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

<script src="/assets/cliente.new.js"></script>

</html>

