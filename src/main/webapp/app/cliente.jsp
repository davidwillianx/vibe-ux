<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.css">
    <link rel="stylesheet" href="<c:url value="/assets/styles.css"/> " >
    <title>Vibe - Cliente - ${cliente.nome}</title>
</head>
<body>
<div class="container">
    <div class="row">


        <div class="col s6 m6 offset-s3 offset-m3">

            <div class="card">

                <div class="card-content">
                    <div class="col m12 s12 vibe-logo">
                        <div class="col m4 s4 offset-m3 offset-s3 ">
                            <img src="<c:url value="/assets/imgs/vibe-logo.png"/>" alt="">
                        </div>
                    </div>

                    <span class="card-title">${cliente.nome}</span>

                    <p>Cpf: ${cliente.cpf}</p>
                    <p>Email: ${cliente.email}</p>
                    <p>
                        Cupons:
                        <a id="add-cupom" clienteId="${cliente.id}" class="waves-effect waves-light btn-flat blue">Adicionar</a>
                    </p>
                    <p>Quantidade: ${cliente.quantidadeCupons}</p>


                    <c:if test="${cliente.quantidadeCupons != 0}">

                        <table>
                            <thead>
                            <th>N&uacute;mero</th>
                            <th>Data</th>
                            </thead>
                            <tbody>
                            <c:forEach items="${cliente.cupons}" var="cupom">
                                <tr>
                                    <td>${cupom.numero}</td>
                                    <td>${cupom.data}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>

                    </c:if>

                    <c:if test="${cliente.quantidadeCupons == 0}">
                        <p>Assim que voc&ecirc; adicionar cupons poder&aacute; v&ecirc;-los aqui</p>
                    </c:if>

                </div>
                <div class="card-action">
                    <a id="remove-cliente" clienteId="${cliente.id}" class="waves-effect waves-light btn red">Remover Cliente</a>
                    <a class="btn-flat waves-effect waves-light pull-right" href="<c:url value="/" />">Procurar Cliente</a>
                </div>

            </div>

        </div>

    </div>

</div>


<div id="modal-info" class="modal">
    <div class="modal-content">
        <h4>Header</h4>
        <p>Description</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
    </div>
</div>





</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-serialize-object/2.5.0/jquery.serialize-object.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

<script src="/assets/cliente.js"></script>

</html>
