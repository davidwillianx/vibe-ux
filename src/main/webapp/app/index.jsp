<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.css">
    <link rel="stylesheet" href="<c:url value="/assets/styles.css"/> " >
    <title>Vibe - Cliente - Procurar</title>
</head>
<body>
<div class="container">
    <div class="row">


        <div class="col s6 m6 offset-s3 offset-m3">

            <div class="card">

                <div class="card-content">
                    <div class="col m12 s12 vibe-logo">
                        <div class="col m4 s4 offset-m3 offset-s3 ">
                            <img src="<c:url value="/assets/imgs/vibe-logo.png"/>" alt="">
                        </div>
                    </div>

                    <form method="GET" action="/q">
                        <div class="row">
                            <input type="text" id="cpfOrEmail" name="cpfOrEmail" class="form-control" placeholder="Digite seu Cpf ou email"/>
                        </div>
                        <button type="submit" id="btn_search" class="btn btn-default" type="button">Procurar</button>
                        <a class="btn-flat waves-effect waves-light pull-right" href="<c:url value="/cliente/new" />">Registrar Cliente</a>
                    </form>

                </div>

            </div>

        </div>

    </div>

</div>



</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-serialize-object/2.5.0/jquery.serialize-object.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

</html>
