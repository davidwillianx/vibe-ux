;(function($, globals){

    var searchClienteForm = (function(){

        var urlRequest = '/cliente';
        var $modal;
        var $formClienteNew;
        var urlRedirectCallback;

        function  init(){

            $formClienteNew = $('#cliente-new')
                .on('submit', registerCliente);

            $modal = $('#modal-info').modal({

                complete: function(){
                    globals.location = urlRedirectCallback;
                }
            });
        }

        function registerCliente(formComponent){
            formComponent.preventDefault();

            var newClienteData = $formClienteNew.serializeObject();

            clienteResourceServer(newClienteData)
                .done(clienteSuccessRegisteredServerResponse)
                .fail(clienteFailureRegisteredServerResponse);
        }

        function clienteSuccessRegisteredServerResponse(clienteRegistered){

            openModal(
                'Sucess!',
                "O Cliente foi registrado com sucesso, voc&ecirc; vera os detalhes em instantes..."
            );

            urlRedirectCallback = '/cliente/' + clienteRegistered.Cpf;
        }

        function clienteFailureRegisteredServerResponse(errorReason){

            var formatedErrorMessage = errorReason.responseText.trim().split('.')
                .filter(function(errorMessage){
                    return /\S/.test(errorMessage);
                }).join(". <br>");

            openModal(
                'Falha!',
                formatedErrorMessage
            );

        }

        function openModal(header, message){
            $modal.find('.modal-content > h4').html(header);
            $modal.find('.modal-content > p').html(message);

            return $modal.modal('open');
        }

        function clienteResourceServer(newClienteData){

           return $.ajax({
                url: urlRequest,
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(newClienteData)
            });
        }

        return {init: init};
    })();


    $(document).ready(searchClienteForm.init());

})(jQuery, window);