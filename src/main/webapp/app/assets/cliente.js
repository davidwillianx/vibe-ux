(function($, globals) {


    var clienteActions = (function(){

        var $btnAddCupon;
        var $btnRemoteCliente;
        var $modal;
        var urlRedirectCallback = '';

        var urlAddCupom = '/cliente/add-cupom/';
        var urlRemoveCliente = '/cliente/remove/'

        function init(){
            $btnAddCupon = $('#add-cupom');
            $btnRemoteCliente = $('#remove-cliente');

            $modal = $('#modal-info').modal({
                complete: function(){
                    globals.location.href = urlRedirectCallback;
                }
            });

            $btnAddCupon.on('click', addCupom);
            $btnRemoteCliente.on('click', removeCliente);
        }

        function addCupom(el){
            el.preventDefault();


            var clienteId = $(this).attr('clienteId');

            requestAddCupom(clienteId)
                .done(successRequestCupom)
                .fail(failRequestCupom);
        }

        function removeCliente(e){
            e.preventDefault();


            if(confirm("Deseja realmente excluir cliente?")){
                var clienteId = $(this).attr('clienteId');

                requestRemoveCliente(clienteId)
                    .done(function(sucessResponse){
                        openModal(
                            "Sucesso",
                            "Cliente removido, em instantes voce sera redirecionado para tela de busca"
                        );
                    })
                    .fail(function(errorReason){
                        openModal(
                            "Error",
                            errorReason.responseText
                        );

                        urlRedirectCallback = '/'

                    });
            }


        }


        function requestRemoveCliente(clienteId){
           return $.ajax({
               url: urlRemoveCliente + clienteId
            });
        }

        function requestAddCupom(clienteId){
           return $.ajax({
                url: urlAddCupom + clienteId
            });
        }

        function successRequestCupom(clienteRequestSucessResponse){
            openModal(
                "Sucesso",
                "Cupom adicionado com sucesso"
            );


            urlRedirectCallback = '/cliente/' +clienteRequestSucessResponse.Cpf;

        }

        function failRequestCupom(){
            openModal(
                "Error",
                "Falha ao adicionar o cupom"
            );
        }

        function openModal(header, message){
            $modal.find('.modal-content > h4').html(header);
            $modal.find('.modal-content > p').html(message);

            return $modal.modal('open');
        }

        return {init : init}
    })();


    $(document).ready(clienteActions.init());

})(jQuery, window);