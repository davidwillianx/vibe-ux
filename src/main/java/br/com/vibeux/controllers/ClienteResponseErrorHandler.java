package br.com.vibeux.controllers;

import br.com.vibeux.handlers.RestUtil;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ClienteResponseErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return RestUtil.isError(clientHttpResponse.getStatusCode());
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

    }

//    @Override
//    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
//        Map<String, String> errorProperties;
//
//        errorProperties = new HashMap<>();
//
//        errorProperties.put("code", clientHttpResponse.getStatusCode().toString());
//        errorProperties.put("body", clientHttpResponse.getBody().toString());
//        errorProperties.put("header", clientHttpResponse.getHeaders().toString());
//
//        throw  new ClienteRequestException(errorProperties);
//
//
//    }

}
