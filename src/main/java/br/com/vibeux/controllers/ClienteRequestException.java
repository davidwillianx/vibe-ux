package br.com.vibeux.controllers;

import java.io.IOException;
import java.util.Map;

public class ClienteRequestException extends IOException {

    private Map errorProperties;

    public ClienteRequestException(Map<String, String> errorProperties) {
        this.errorProperties = errorProperties;
    }

    public Map getErrorProperties() {
        return errorProperties;
    }

    public void setErrorProperties(Map errorProperties) {
        this.errorProperties = errorProperties;
    }
}
