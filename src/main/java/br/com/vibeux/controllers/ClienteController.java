package br.com.vibeux.controllers;

import br.com.vibeux.converters.Cliente;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/")
public class ClienteController {

    private RestTemplate apiRequest;
    private String mainApiURL = "http://selecao2017.vibedesenv.com/Cliente/";
    private Map<String, String> params;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String CPF_PATTERN = "^[0-9]{11}$";

    private Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    private Pattern cpfPattern = Pattern.compile(CPF_PATTERN);

    @RequestMapping
    public String home(){
        return "index";
    }

    @RequestMapping(value = "/q", method = RequestMethod.GET, produces = "application/json")
    public ModelAndView findByCpfOrEmail(
            @RequestParam("cpfOrEmail") String cpfOrEmail, RedirectAttributes redirectAttributes){

        String apiURL = "http://selecao2017.vibedesenv.com/Cliente/{cpfOrEmail}";

        apiRequest = new RestTemplate();
        params = new HashMap<>();
        params.put("cpfOrEmail", cpfOrEmail);

        try{
           Cliente clienteFound = apiRequest.getForObject(apiURL, Cliente.class, params);
            return new ModelAndView("redirect:/cliente/" + clienteFound.getCpf());

        }catch (HttpClientErrorException error){

            Cliente cliente = new Cliente();

            if(error.getStatusCode().equals(HttpStatus.NOT_FOUND)){

                cliente = clienteViewCpfEmailDataVirify(cpfOrEmail);
            }

            redirectAttributes.addFlashAttribute("cliente", cliente);

            return new ModelAndView("redirect:/cliente/new");
        }

    }


    @RequestMapping("/cliente/{cpfOrEmail}")
    public ModelAndView clienteDetails(@PathVariable String cpfOrEmail, RedirectAttributes redirectAttributes){

        String apiURL = "http://selecao2017.vibedesenv.com/Cliente/{cpfOrEmail}";
        apiRequest = new RestTemplate();
        params = new HashMap<>();
        params.put("cpfOrEmail", cpfOrEmail);

        try {

            Cliente clienteFound = apiRequest.getForObject(apiURL, Cliente.class, params);
            return new ModelAndView("cliente", "cliente", clienteFound);

        }catch (HttpClientErrorException error){

            Cliente cliente = new Cliente();

            if(error.getStatusCode().equals(HttpStatus.NOT_FOUND)){

                cliente = clienteViewCpfEmailDataVirify(cpfOrEmail);
            }

            redirectAttributes.addFlashAttribute("cliente", cliente);

            return new ModelAndView("redirect:/cliente/new");
        }
    }

    private Cliente clienteViewCpfEmailDataVirify(
            String cpfOrEmail
    ){

        Cliente cliente = new Cliente();


        if(emailPattern.matcher(cpfOrEmail).matches()){
            cliente.setEmail(cpfOrEmail);
        }

        if(cpfPattern.matcher(cpfOrEmail).matches()){
            cliente.setCpf(cpfOrEmail);
        }

        return cliente;

    }

    @RequestMapping("/cliente/add-cupom/{clienteId}")
    public ResponseEntity clienteAddCupom(@PathVariable int clienteId){

        String apiUrl = mainApiURL + "{clienteId}";

        params = new HashMap<>();
        apiRequest = new RestTemplate();
        params.put("clienteId", new Integer(clienteId).toString());

        try{

            HttpEntity<Cliente> serverEntityResponse = apiRequest.exchange(apiUrl, HttpMethod.PUT, null, Cliente.class, params);
            return new ResponseEntity(serverEntityResponse.getBody(), HttpStatus.OK);

        }catch (HttpClientErrorException error){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }



    @RequestMapping("/cliente/new")
    public ModelAndView newCliente(){
       return new ModelAndView("cliente.new") ;
    }

    @RequestMapping(value = "/cliente", method = RequestMethod.POST)
    public ResponseEntity newCliente(@RequestBody Cliente cliente){

        apiRequest = new RestTemplate();
        HttpEntity<Cliente> clienteToRegister = new HttpEntity<>(cliente);

        try{
            Cliente clienteRegistred = apiRequest.postForObject(mainApiURL, clienteToRegister, Cliente.class);
            return new ResponseEntity( clienteRegistred , HttpStatus.OK );
        }catch (HttpClientErrorException error){
            return new ResponseEntity(error.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping("/cliente/remove/{clienteId}")
    public ResponseEntity deleteCliente(@PathVariable int clienteId){
        String apiUrl = mainApiURL + "{clienteId}";
        apiRequest = new RestTemplate();
        params = new HashMap<>();
        params.put("clienteId", new Integer(clienteId).toString());

        try{
            apiRequest.delete(apiUrl,params);
            return new ResponseEntity(HttpStatus.OK);
        }catch (HttpClientErrorException error){
            return new ResponseEntity(error.getResponseBodyAsString(), error.getStatusCode());
        }
    }
}
