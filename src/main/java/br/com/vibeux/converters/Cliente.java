package br.com.vibeux.converters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Cliente {

    @JsonProperty("Id")
    private int id;
    @JsonProperty("Nome")
    private String nome;
    @JsonProperty("Cpf")
    private String cpf;
    @JsonProperty("Email")
    private String email;
    @JsonProperty("Cupons")
    private List<Cupom> cupons;

    @JsonProperty("QuantidadeCupons")
    private int quantidadeCupons;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Cupom> getCupons(){
        return cupons;
    }

    public void setCupons(List<Cupom> cupons) {
        this.cupons = cupons;
    }

    public int getQuantidadeCupons() {
        return quantidadeCupons;
    }

    public void setQuantidadeCupons(int quantidadeCupons) {
        this.quantidadeCupons = quantidadeCupons;
    }
}
