package br.com.vibeux.converters;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cupom {

    @JsonProperty("Numero")
    private String numero;
    @JsonProperty("Data")
    private String data;


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "Cupom{" +
                "numero='" + numero + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
